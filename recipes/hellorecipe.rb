directory node['dir'] do
  action :create
end

file 'hello.txt' do
  content '<html>This is a placeholder for the home page.</html>'
  path node['dir']
end
